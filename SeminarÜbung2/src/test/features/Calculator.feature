Feature: Multiplikation
  Als Anwender möchte ich zwei ganze Zeilen miteinander multiplizieren
  Scenario: Multiplikation von zwei positiven Ganzzahlen
    Given Ich starte Multiplikation
    When Ich 3 als Operand1 und 4 als Operand2 eingebe
    Then erhalte ich 12 als Ergebnis
  Scenario: Multiplikation von einer positiven und einer negativen Zahl
    Given Ich starte Multiplikation
    When Ich 3 als Operand1 und -4 als Operand2 eingebe
    Then erhalte ich -12 als Ergebnis
  Scenario: Multiplikation einer positiven und 0
    Given Ich starte Multiplikation
    When Ich 3 als Operand1 und 0 als Operand2 eingebe
    Then erhalte ich 0 als Ergebnis