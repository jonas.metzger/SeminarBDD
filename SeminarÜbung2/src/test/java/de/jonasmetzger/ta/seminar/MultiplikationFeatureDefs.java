package de.jonasmetzger.ta.seminar;

import cucumber.api.java.en.*;
import org.junit.Assert;

public class MultiplikationFeatureDefs {
    private CalculatorApplication calculatorApplication;

    @Given("^Ich starte Multiplikation$")
    public void ichStarteMultiplikation() {
        calculatorApplication = new CalculatorApplication();
    }

    @When("^Ich (\\d+) als Operand1 und (-?\\d+) als Operand2 eingebe$")
    public void ichAlsOperandUndAlsOperandEingebe(int arg0, int arg1) throws Exception {
        calculatorApplication.multiply(arg0, arg1);
    }

    @Then("^erhalte ich (-?\\d+) als Ergebnis$")
    public void erhalteIchAlsErgebnis(int arg0) {
        Assert.assertEquals(arg0, calculatorApplication.getErgebnis());
    }
}
