package de.jonasmetzger.ta.seminar;

public class CalculatorApplication {

    private int ergebnis;

    public void multiply(int operand1, int operand2) throws Exception {
        this.ergebnis = operand1 * operand2;
    }

    public int getErgebnis() {
        return this.ergebnis;
    }

}
