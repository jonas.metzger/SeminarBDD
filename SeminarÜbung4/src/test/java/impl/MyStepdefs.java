package impl;

import cucumber.api.Pending;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class MyStepdefs {
    private ChromeDriver chromeDriver;

    @Given("^Ich starte den \"([^\"]*)\" Browser$")
    public void ichStarteDenBrowser(String arg0) {
        System.setProperty("webdriver.chrome.driver", "C:\\temp\\chromedriver.exe");
        chromeDriver = new ChromeDriver();
    }

    @And("^Ich navigiere zu \"([^\"]*)\"$")
    public void ichNavigiereZu(String url) {
        chromeDriver.navigate().to(url);
    }

    @When("^Ich gebe Suchbegriff \"([^\"]*)\" ein$")
    public void ichGebeSuchbegriffEin(String arg0) {
        chromeDriver.findElement(By.xpath("//input[@name='q']")).sendKeys(arg0);
    }

    @And("^Ich klicke auf Suche$")
    public void ichKlickeAufSuche() {
        chromeDriver.findElement(By.xpath("//div[@class='VlcLAe']//input[@name='btnK']")).click();
    }

    @Then("^\"([^\"]*)\" ist in der Trefferliste von Google$")
    public void istInDerTrefferlisteVonGoogle(String sucheTitel) {
        chromeDriver.findElementByXPath("//h3[contains(text(),'" + sucheTitel + "')]");
    }

    @After
    public void cleanup() {
        if (chromeDriver != null)
            chromeDriver.close();
    }
}
