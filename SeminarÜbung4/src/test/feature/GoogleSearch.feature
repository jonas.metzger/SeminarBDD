Feature: Simple Google Search
  Als Anwender möchte ich bei Google nach einen Begriff googeln
  Scenario: Google Search Nach Wikipedia
    Given Ich starte den "Chrome" Browser
    And Ich navigiere zu "https://google.de/"
    When Ich gebe Suchbegriff "Hallo Welt" ein
    And Ich klicke auf Suche
    Then "Hallo Welt – Wikipedia" ist in der Trefferliste von Google
  Scenario: Google Suche Testautomatisierung
    Given Ich starte den "Chrome" Browser
    And Ich navigiere zu "https://google.de"
    When Ich gebe Suchbegriff "Testautomatisierung" ein
    And Ich klicke auf Suche
    Then "Testautomatisierung - Expertenwissen, Definition, Tutorials" ist in der Trefferliste von Google