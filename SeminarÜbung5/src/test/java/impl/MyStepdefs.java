package impl;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyStepdefs {
    private ChromeDriver chromeDriver;
    private String shopUrl = "https://demoproject.simplytest.de";

    @Given("^I open web browser $")
    public void iOpenWebBrowser() {
        System.setProperty("webdriver.chrome.driver", "C:\\temp\\chromedriver.exe");
        chromeDriver = new ChromeDriver();
    }

    @When("^I navigate to the Online Shop$")
    public void iNavigateToTheOnlineShop() {
        chromeDriver.navigate().to(shopUrl);
    }

    @Then("^I should see the shop dashboard with the title \"([^\"]*)\" $")
    public void iShouldSeeTheShopDashboardWithTheTitle(String websiteTitle) {
        Assert.assertEquals(chromeDriver.findElementByXPath("//h1[@class='woocommerce-products-header__title page-title']").getText(), websiteTitle);
    }

    @And("^I should see offered article \"([^\"]*)\"$")
    public void iShouldSeeOfferedArticle(String articleName) {
        Assert.assertEquals(chromeDriver.findElementByXPath("//h2[contains(text(),'" + articleName + "')]").getText(), articleName);
    }

    @And("^I should see the shopping card with (\\d+) articles?")
    public void iShouldSeeTheShoppingCardWithArticles(int anzahlArtikel) {
        WebElement webElement = chromeDriver.findElementByXPath("//*[@class=\"cart-contents\"]//*[@class=\"count\"]");
        Assert.assertEquals(anzahlArtikel + " Artikel", webElement.getText());
    }

    @Given("^I'm on the dashboard page of the Online Shop $")
    public void iMOnTheDashboardPageOfTheOnlineShop() {
        iOpenWebBrowser();
        iNavigateToTheOnlineShop();
    }

    @When("^I add the article \"([^\"]*)\" to the shopping card $")
    public void iAddTheArticleToTheShoppingCard(String artikel) {
        chromeDriver.findElementByXPath("//a[@data-product_sku=\"woo-" + artikel.toLowerCase() + "\"]").click();
        WebDriverWait webDriverWait = new WebDriverWait(chromeDriver, 10);
        webDriverWait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//a[@title=\"Warenkorb anzeigen\"]")));
    }

    @Given("^I'm on the shopping card page with added (\\d+) article \"([^\"]*)\" $")
    public void iMOnTheShoppingCardPageWithAddedArticle(int anzahlArtikel, String artikelName) {
        iOpenWebBrowser();
        iNavigateToTheOnlineShop();
        iAddTheArticleToTheShoppingCard(artikelName);
        iShouldSeeTheShoppingCardWithArticles(anzahlArtikel);
        // Repository mit Kostanten implementieren

    }

    @When("^I increase article amount to (\\d+) $")
    public void iIncreaseArticleAmountTo(int arg0) {

    }

    @And("^I refresh the shopping card $")
    public void iRefreshTheShoppingCard() {

    }

    @Then("^I should see updated total price of € \"([^\"]*)\" $")
    public void iShouldSeeUpdatedTotalPriceOf€(String arg0) {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Given("^I'm on the shopping card page with added (\\d+) articles \"([^\"]*)\" $")
    public void iMOnTheShoppingCardPageWithAddedArticles(int arg0, String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^I navigate to the order page $")
    public void iNavigateToTheOrderPage() {

    }

    @And("^I enter first name $")
    public void iEnterFirstName() {

    }

    @And("^I enter last name $")
    public void iEnterLastName() {

    }

    @And("^I enter street $")
    public void iEnterStreet() {

    }

    @And("^I enter zip code $")
    public void iEnterZipCode() {

    }

    @And("^I enter city $")
    public void iEnterCity() {

    }

    @And("^I enter phone $")
    public void iEnterPhone() {

    }

    @And("^I enter email $")
    public void iEnterEmail() {

    }

    @And("^I submit the order $")
    public void iSubmitTheOrder() {

    }

    @Then("^I should see approved order message \"([^\"]*)\" $")
    public void iShouldSeeApprovedOrderMessage(String arg0) {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @After
    public void closeDriver() throws InterruptedException {
        if (chromeDriver != null)
            Thread.sleep(3000);
            chromeDriver.close();
    }
}
