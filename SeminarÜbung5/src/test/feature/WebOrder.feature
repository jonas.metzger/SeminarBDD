@run
Feature: Online Shop Order Customer Journey As a user I want to be able to order a new arcticle without registration,
  visiting the online shop, selecting desired article,
  adding it twice to the shopping card and
  filling-in the order formular on checkout as the last step 
   
  Scenario: Display Online Shop Dashboard with articles 
    Given I open web browser 
    When I navigate to the Online Shop
    Then I should see the shop dashboard with the title "Shop" 
    And I should see offered article "Album"
    And I should see the shopping card with 0 articles

  Scenario: Add article to shopping card 
    Given I'm on the dashboard page of the Online Shop 
    When I add the article "Album" to the shopping card 
    Then I should see the shopping card with 1 article

  Scenario: Increase article amount in shopping card 
    Given I'm on the shopping card page with added 1 article "Album" 
    When I increase article amount to 2 
    And I refresh the shopping card 
    Then I should see updated total price of € "30,00" 

  Scenario: Fill-in and send order 
    Given I'm on the shopping card page with added 2 articles "Album" 
    When I navigate to the order page 
    And I enter first name 
    And I enter last name 
    And I enter street 
    And I enter zip code 
    And I enter city 
    And I enter phone 
    And I enter email 
    And I submit the order 
    Then I should see approved order message "Bestellung erhalten" 
