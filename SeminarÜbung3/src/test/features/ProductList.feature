Feature: Auflisten von Produkten im Webshop
  Als API-Nutzer möchte ich die Produkte im Webshop auflisten können
  Scenario: Produktliste ermitteln
    Given Produktservice ist erreichbar
    And "Apples" sind als Produkt verfügbar
    When Produktliste wird aufgerufen
    Then wird ein Response Status Code 200 erwartet
    And Response erhält ein Produkt "Apples"
    And Response erhält ein Produkt "Mango fresh"