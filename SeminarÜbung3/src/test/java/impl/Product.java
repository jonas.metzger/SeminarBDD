package impl;

public class Product {
    private String name;
    private String product_url;

    public Product() {}

    public Product(String name, String product_url) {
        this.name = name;
        this.product_url = product_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProduct_url() {
        return product_url;
    }

    public void setProduct_url(String product_url) {
        this.product_url = product_url;
    }
}
