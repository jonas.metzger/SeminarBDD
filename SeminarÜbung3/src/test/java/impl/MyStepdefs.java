package impl;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

import static org.hamcrest.Matchers.*;

import java.io.Serializable;
import java.util.List;

import static io.restassured.RestAssured.*;

public class MyStepdefs {
    private RequestSpecification requestSpecification;
    private Response response;

    @Given("^Produktservice ist erreichbar$")
    public void produktserviceIstErreichbar() {
        requestSpecification = given().contentType(ContentType.JSON);
    }

    @And("^\"([^\"]*)\" sind als Produkt verfügbar$")
    public void sindAlsProduktVerfügbar(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }

    @When("^Produktliste wird aufgerufen$")
    public void produktlisteWirdAufgerufen() {
        response = requestSpecification.when().get("https://api.predic8.de:443/shop/products/");
    }

    @Then("^wird ein Response Status Code (\\d+) erwartet$")
    public void wirdEinResponseStatusCodeErwartet(int arg0) {
        Assert.assertEquals(arg0, response.statusCode());
        response.print();
    }

    @And("^Response erhält ein Produkt \"([^\"]*)\"$")
    public void responseErhältEinProdukt(String arg0) {

        Assert.assertTrue(response.body().jsonPath().getList("products", Product.class).stream().anyMatch(product -> product.getName().equals(arg0)));
    }
}
